import React, { useState } from 'react';
import { IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonInput, IonButton } from '@ionic/react';
import { Pin } from '../data/pins';

interface FormCreatePinProps {
  onPinCreate: (newPin: Pin) => void;
}

const FormCreatePin: React.FC<FormCreatePinProps> = ({ onPinCreate }) => {
  const [newPin, setNewPin] = useState<Pin>({
    id: 0,
    title: '',
    content: '',
    source: '',
    tags: [],
    creationDate: ''
  });

  const addPin = () => {
    onPinCreate(newPin);
    setNewPin({
      id: 0,
      title: '',
      content: '',
      source: '',
      tags: [],
      creationDate: ''
    });
  };

  return (
    <IonCard>
      <IonCardHeader>
        <IonCardTitle>Nouvelle Épingle</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <IonInput
          value={newPin.title}
          placeholder="Titre"
          onIonChange={(e) => setNewPin({ ...newPin, title: e.detail.value! })}
        />
        <IonInput
          value={newPin.content}
          placeholder="Description"
          onIonChange={(e) => setNewPin({ ...newPin, content: e.detail.value! })}
        />
        <IonButton expand="full" onClick={addPin}>Créer</IonButton>
      </IonCardContent>
    </IonCard>
  );
};

export default FormCreatePin;
