import {
  IonItem,
  IonLabel,
  IonNote,
  IonChip,
  IonButton, // Importez IonButton
} from '@ionic/react';
import { Pin } from '../data/pins';
import './MessageListItem.css';

interface QuoteListItemProps {
  pin: Pin;
  onDelete: (id: number) => void; // Fonction pour supprimer une épingle
}

const QuoteListItem: React.FC<QuoteListItemProps> = ({ pin, onDelete }) => {
  const handleDelete = (e: React.MouseEvent, pinId: number) => {
    e.preventDefault();
    e.stopPropagation();
    onDelete(pinId);
  };

  return (
    <IonItem routerLink={`/pin/${pin.id}`} detail={false}>
      <div slot="start" className="dot dot-unread"></div>
      <IonLabel className="ion-text-wrap">
        <h2>
          {pin.title}
          <span className="date">
            {pin.tags.map((tag, index) => (
              <IonChip key={index}>{tag}</IonChip>
            ))}
          </span>
        </h2>
        <p>{pin.content}</p>
      </IonLabel>
      <IonButton onClick={(e) => handleDelete(e, pin.id)}>Supprimer</IonButton> {/* Bouton de suppression */}
    </IonItem>
  );
};

export default QuoteListItem;
