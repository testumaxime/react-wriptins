import QuoteListItem from '../components/PinListItem';
import FormCreatePin from '../components/FormCreatePin'; 
import { useState } from 'react';
import { Pin, getPins } from '../data/pins';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter
} from '@ionic/react';
import './Home.css';
import { useAuth } from '../auth';

const Home: React.FC = () => {
  const { loggedIn, setLoggedIn } = useAuth();
  const [pins, setPins] = useState<Pin[]>([]);

  const handlePinCreate = (newPin: Pin) => {
    const currentDate = new Date();
    const formattedDate = `${currentDate.getFullYear()}-${(currentDate.getMonth() + 1).toString().padStart(2, '0')}-${currentDate.getDate().toString().padStart(2, '0')}`;
  
    const newPinWithIdAndDate = {
      ...newPin,
      id: pins.length > 0 ? Math.max(...pins.map(pin => pin.id)) + 1 : 1, // Calculez le nouvel ID
      creationDate: formattedDate // Utilisez la date formatée actuelle
    };
      setPins([...pins, newPinWithIdAndDate]);
  }

  const handleDeletePin = (pinId: number) => {
    // Créez une nouvelle liste d'épingles en excluant celle avec l'ID pinId
    const updatedPins = pins.filter((pin) => pin.id !== pinId);
    // Mettez à jour l'état des épingles avec la nouvelle liste
    setPins(updatedPins);
  };

  useIonViewWillEnter(() => {
    const pins = getPins();
    setPins(pins);
  });

  const refresh = (e: CustomEvent) => {
    setTimeout(() => {
      e.detail.complete();
    }, 3000);
  };

  const handleLogout = () => {
    setLoggedIn(false);

    // Effectuez une redirection vers la page de connexion (par exemple, '/login')
    // history.push('/login');
  };

  return (
    <IonPage id="home-page">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Inbox</IonTitle>
          {loggedIn ? (
            <IonButton slot="end" onClick={handleLogout}>Déconnexion</IonButton>
            ) : (<p slot="end"></p>)
          }
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonRefresher slot="fixed" onIonRefresh={refresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">
              Inbox
            </IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          {pins.map(p => <QuoteListItem key={p.id} pin={p} onDelete={handleDeletePin}/>)}
        </IonList>
        <FormCreatePin onPinCreate={handlePinCreate}/>
      </IonContent>
    </IonPage>
  );
};

export default Home;
