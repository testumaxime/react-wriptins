import React, { useState } from 'react';
import { useAuth } from '../auth';
import { IonButton, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useHistory } from 'react-router-dom';
import useStoredState from "../hooks";

function LoginForm() {
  const { setLoggedIn } = useAuth();
  const [loggedIn, setLoggedInLocally] = useStoredState('loggedIn', false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();

  const handleLogin = () => {
    if (username === 'user' && password === 'password') {
      setLoggedIn(true);
      setLoggedInLocally(true);
     // history.push('/home');
    } else {
      setError('Nom d\'utilisateur ou mot de passe incorrect.');
    }
  };

  return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Connexion</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <form onSubmit={handleLogin}>
            <IonItem>
              <IonLabel position="floating">Identifiant</IonLabel>
              <IonInput
                  type="text"
                  value={username || ''}
                  onIonChange={(e) => setUsername(e.detail.value || '')}
              ></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Mot de passe</IonLabel>
              <IonInput
                  type="password"
                  value={password || ''}
                  onIonChange={(e) => setPassword(e.detail.value || '')}
              ></IonInput>
            </IonItem>
            <IonButton expand="full" onClick={handleLogin}>Connexion</IonButton>
          </form>
          {error && <p style={{ color: 'red' }}>{error}</p>}
        </IonContent>
      </IonPage>
  );
}

export default LoginForm;
