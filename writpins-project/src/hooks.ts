import { useState, useEffect } from 'react';

function useStoredState(key: any, initialValue: any) {
  // Fonction pour récupérer la valeur stockée dans localStorage lors de la première exécution.
  const getStoredValue = () => {
    try {
      const storedValue = localStorage.getItem(key);
      return storedValue ? JSON.parse(storedValue) : initialValue;
    } catch (error) {
      console.error(`Error loading state from localStorage: ${error}`);
      return initialValue;
    }
  };

  // État local initial avec récupération de la valeur stockée.
  const [state, setState] = useState(getStoredValue);

  function setStoreState(state: any){
        try {
            localStorage.setItem(key, JSON.stringify(state));
            setState(state);
        } catch (error) {
            console.error(`Error saving state to localStorage: ${error}`);
        }
  }

  return [state, setStoreState];
}

export default useStoredState;