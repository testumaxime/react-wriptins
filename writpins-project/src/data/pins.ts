export interface Pin {
    id: number;
    title: string;
    content: string;
    source: string;
    tags: string[];
    creationDate: string;
}

const pins: Pin[] = [
    {
        id: 0,
        title: 'How to make a good cup of coffee',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod, nisl vitae tincidunt ultricies, nunc nisl ultricies nunc, vitae aliquam nisl nunc vitae nisl. Sed vitae nisl eget nisl aliquam aliquet. Sed vitae nisl eget nisl aliquam aliquet.',
        source: 'https://www.youtube.com/watch?v=6Q0eCG0rwxY', 
        tags: ['coffee', 'tutorial'],
        creationDate: '2020-01-01'
    },
    {
        id: 1,
        title: '5 ways to have a better sleep',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod, nisl vitae tincidunt ultricies, nunc nisl ultricies nunc, vitae aliquam nisl nunc vitae nisl. Sed vitae nisl eget nisl aliquam aliquet. Sed vitae nisl eget nisl aliquam aliquet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod, nisl vitae tincidunt ultricies, nunc nisl ultricies nunc, vitae aliquam nisl nunc vitae nisl. Sed vitae nisl eget nisl aliquam aliquet. Sed vitae nisl eget nisl aliquam aliquet.',
        source: 'https://www.youtube.com/watch?v=6Q0eCG0rwxY',
        tags: ['sleep', 'tutorial'],
        creationDate: '2020-01-01'
    },
    {
        id: 2,
        title: 'Best places to visit in 2020',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod, nisl vitae tincidunt ultricies, nunc nisl ultricies nunc, vitae aliquam nisl nunc vitae nisl. Sed vitae nisl eget nisl aliquam aliquet. Sed vitae nisl eget nisl aliquam aliquet.',
        source: 'https://www.youtube.com/watch?v=6Q0eCG0rwxY',
        tags: ['travel', 'tutorial'],
        creationDate: '2020-01-01'
    }
];

export const getPins = () => pins;

export const getPin = (id: number) => pins.find(m => m.id === id);